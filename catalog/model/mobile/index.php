<?php
class ModelMobileIndex extends Model
{

    const PRODUCT_IMAGE_SIZE = 1080;

    const SELLER_IMAGE_SIZE = 256;


    public function getHomeModules()
    {
        return $this->db->query("SELECT * FROM " . DB_PREFIX . "mobile_home
            WHERE status = '1'
            ORDER BY sort_order")->rows;
    }


    public function getFormattedProduct($product)
    {
        $this->load->model('journal3/image');
        $this->load->model('tool/image');

        if ($product['image']) {
            if (defined('JOURNAL3_ACTIVE')) {
                $image = $this->model_journal3_image->resize($product['image'], self::PRODUCT_IMAGE_SIZE, self::PRODUCT_IMAGE_SIZE, $this->journal3->settings->get('image_dimensions_product.resize'));
            } else {
                $image = $this->model_tool_image->resize($product['image'], self::PRODUCT_IMAGE_SIZE, self::PRODUCT_IMAGE_SIZE);
            }
        } else {
            $image = $this->model_tool_image->resize('placeholder.png', self::PRODUCT_IMAGE_SIZE, self::PRODUCT_IMAGE_SIZE);
        }

        $price = $this->customer->isLogged() || !$this->config->get('config_customer_price') ?
            $this->currency->format($this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']) :
            false;

        $special = (float) $product['special'] ?
            $this->currency->format($this->tax->calculate($product['special'], $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']) :
            false;

        return array_merge($product, [
            'price'       => str_replace('decimal_point', '.', $price),
            'special'     => str_replace('decimal_point', '.', $special),
            'description' => html_entity_decode($product['description'], ENT_QUOTES, 'UTF-8'),
            'image'       => $image,
        ]);
    }


    public function getFormattedSeller($seller)
    {
        $this->load->model('tool/image');
        $locality = explode(',', $seller['companylocality'] ?? '');

        return array_merge($seller, [
            'companylogo'        => isset($seller['companylogo']) ? $this->model_tool_image->resize($seller['companylogo'], self::SELLER_IMAGE_SIZE, self::SELLER_IMAGE_SIZE) : '',
            'shortprofile'       => html_entity_decode($seller['shortprofile'] ?? '', ENT_QUOTES, 'UTF-8'),
            'companydescription' => html_entity_decode($seller['companydescription'] ?? '', ENT_QUOTES, 'UTF-8'),
            'location'           => [
                floatval(trim($locality[0] ?? '')),
                floatval(trim($locality[1] ?? '')),
            ],
        ]);
    }
}
