<?php

class ControllerMobileAccount extends MobileController
{

    const DEFAULT_COUNTRY_ID = 229; // Venezuela

    const ORDERS_PER_PAGE = 5;

    const DATE_FORMAT = 'd/m/Y';


    /**
     * Register endpoint.
     * Fields: firstname, lastname, email, telephone, password, confirm
     */
    public function register()
    {
        $this->load->language('account/register');
        $this->load->model('account/customer');

        $this->data['errors'] = [];

        if ((utf8_strlen(trim($this->request->post['firstname'])) < 1) ||
            (utf8_strlen(trim($this->request->post['firstname'])) > 32)) {
			$this->data['errors']['firstname'] = $this->language->get('error_firstname');
		}

        if ((utf8_strlen(trim($this->request->post['lastname'])) < 1) ||
            (utf8_strlen(trim($this->request->post['lastname'])) > 32)) {
			$this->data['errors']['lastname'] = $this->language->get('error_lastname');
		}

        if ((utf8_strlen($this->request->post['email']) > 96) ||
            !filter_var($this->request->post['email'], FILTER_VALIDATE_EMAIL)) {
			$this->data['errors']['email'] = $this->language->get('error_email');
		}

		if ($this->model_account_customer->getTotalCustomersByEmail($this->request->post['email'])) {
			$this->data['errors']['email'] = $this->language->get('error_exists');
		}

        if ((utf8_strlen($this->request->post['telephone']) < 3) ||
            (utf8_strlen($this->request->post['telephone']) > 32)) {
			$this->data['errors']['telephone'] = $this->language->get('error_telephone');
		}

        if ((utf8_strlen(html_entity_decode($this->request->post['password'], ENT_QUOTES, 'UTF-8')) < 4) ||
            (utf8_strlen(html_entity_decode($this->request->post['password'], ENT_QUOTES, 'UTF-8')) > 40)) {
			$this->data['errors']['password'] = $this->language->get('error_password');
		}

		if ($this->request->post['confirm'] != $this->request->post['password']) {
			$this->data['errors']['confirm'] = $this->language->get('error_confirm');
        }

        if (!empty($this->data['errors'])) {
            $this->send();
            return;
        }

        $this->data['errors'] = false;
        $this->request->post['affiliate'] = false;
        $this->request->post['newsletter'] = false;
        $this->request->post['status'] = true;
        $this->request->post['safe'] = false;

        $this->model_account_customer->addCustomer($this->request->post);
        $this->model_account_customer->deleteLoginAttempts($this->request->post['email']);
        $this->customer->login($this->request->post['email'], $this->request->post['password']);

        // Wishlist
        if (isset($this->session->data['wishlist']) && is_array($this->session->data['wishlist'])) {
            $this->load->model('account/wishlist');

            foreach ($this->session->data['wishlist'] as $key => $product_id) {
                $this->model_account_wishlist->addWishlist($product_id);
                unset($this->session->data['wishlist'][$key]);
            }
        }

        $this->getInfo();
    }


    /**
     * Register endpoint.
     * Fields: firstname, lastname, telephone
     */
    public function edit()
    {
        $this->load->language('account/register');
        $this->load->model('account/customer');

        $this->data['errors'] = [];

        if (!$this->customer->isLogged()) {
            $this->load->language('account/edit');
            $this->data['errors']['logged'] = $this->language->get('error_logged');
        }

        if ((utf8_strlen(trim($this->request->post['firstname'])) < 1) ||
            (utf8_strlen(trim($this->request->post['firstname'])) > 32)) {
			$this->data['errors']['firstname'] = $this->language->get('error_firstname');
		}

        if ((utf8_strlen(trim($this->request->post['lastname'])) < 1) ||
            (utf8_strlen(trim($this->request->post['lastname'])) > 32)) {
			$this->data['errors']['lastname'] = $this->language->get('error_lastname');
		}

        if ((utf8_strlen($this->request->post['telephone']) < 3) ||
            (utf8_strlen($this->request->post['telephone']) > 32)) {
			$this->data['errors']['telephone'] = $this->language->get('error_telephone');
		}

        if (!empty($this->data['errors'])) {
            $this->send();
            return;
        }

        $this->data['errors'] = false;
        $this->request->post['email'] = $this->customer->getEmail();
        $this->model_account_customer->editCustomer($this->customer->getId(), $this->request->post);
        $this->send();
    }


    /**
     * Login endpoint.
     * Fields: email, password
     */
    public function login()
    {
        $this->load->language('account/login');
        $this->load->model('account/customer');
        $info = $this->model_account_customer->getLoginAttempts($this->request->post['email']);

        if ($info && $info['total'] >= $this->config->get('config_login_attempts') &&
            strtotime('-1 hour') < strtotime($info['date_modified'])) {
			$this->data['error'] = $this->language->get('error_attempts');
        }

        $info = $this->model_account_customer->getCustomerByEmail($this->request->post['email']);
		if ($info && !$info['status']) {
            $this->data['error'] = $this->language->get('error_approved');
        }

        if (!($this->data['error'] ?? false)) {
            if (!$this->customer->login($this->request->post['email'], $this->request->post['password'])) {
                $this->data['error'] = $this->language->get('error_login');
                $this->model_account_customer->addLoginAttempt($this->request->post['email']);
            } else {
                $this->model_account_customer->deleteLoginAttempts($this->request->post['email']);
                $this->getInfo();
                return;
            }
        }

        $this->send();
    }


    /**
     * Get info endpoint.
     */
    public function getInfo()
    {
        $this->data['data'] = [
            'firstname'     => $this->customer->getFirstName(),
            'lastname'      => $this->customer->getLastName(),
            'email'         => $this->customer->getEmail(),
            'telephone'     => $this->customer->getTelephone(),
            'currency_code' => $this->session->data['currency'],
        ];

        $this->send();
    }


    /**
     * Get zones endpoint.
     */
    public function getZones()
    {
        $this->load->model('localisation/zone');
        $this->data['zones'] = $this->model_localisation_zone->getZonesByCountryId(self::DEFAULT_COUNTRY_ID);
        $this->send();
    }


    /**
     * Get addresses endpoint.
     */
    public function getAddresses()
    {
        $this->load->model('account/address');

        $this->data['addresses'] = array_values($this->model_account_address->getAddresses());
        $this->send();
    }


    /**
     * Get address endpoint.
     * Fields: address_id
     */
    public function getAddress()
    {
        $this->load->model('account/address');
        $this->data['address'] = $this->model_account_address->getAddress($this->request->get['address_id']);
        $this->send();
    }


    /**
     * Add address endpoint.
     * Fields: firstname, lastname, address_1, address_2, city, postcode, zone_id, default
     */
    public function addAddress()
    {
        $this->load->language('account/address');
        $this->data['errors'] = $this->validateAddress();

        if (!empty($this->data['errors'])) {
            $this->send();
            return;
        }

        $this->load->model('account/address');
        $this->request->post['company'] = '';
        $this->request->post['country_id'] = self::DEFAULT_COUNTRY_ID;

        $this->model_account_address->addAddress($this->customer->getId(), $this->request->post);
        $this->send();
    }


    /**
     * Edit address endpoint.
     * Fields: firstname, lastname, address_1, address_2, city, postcode, zone_id, default
     */
    public function editAddress()
    {
        $this->load->language('account/address');
        $this->data['errors'] = $this->validateAddress();

        if (!empty($this->data['errors'])) {
            $this->send();
            return;
        }

        $this->load->model('account/address');

        // Default Shipping Address
        if (isset($this->session->data['shipping_address']['address_id']) &&
            ($this->request->get['address_id'] == $this->session->data['shipping_address']['address_id'])) {
            $this->session->data['shipping_address'] = $this->model_account_address->getAddress($this->request->get['address_id']);

            unset($this->session->data['shipping_method']);
            unset($this->session->data['shipping_methods']);
        }

        // Default Payment Address
        if (isset($this->session->data['payment_address']['address_id']) &&
            ($this->request->get['address_id'] == $this->session->data['payment_address']['address_id'])) {
            $this->session->data['payment_address'] = $this->model_account_address->getAddress($this->request->get['address_id']);

            unset($this->session->data['payment_method']);
            unset($this->session->data['payment_methods']);
        }

        $this->request->post['company'] = '';
        $this->request->post['country_id'] = self::DEFAULT_COUNTRY_ID;

        $this->model_account_address->editAddress($this->request->get['address_id'], $this->request->post);
        $this->send();
    }


    /**
     * Get orders endpoint.
     * Fields: page
     */
    public function getOrders()
    {
        $this->load->model('account/order');
        $page = $this->request->get['page'] ?? 1;
		$order_total = $this->model_account_order->getTotalOrders();
		$results = $this->model_account_order->getOrders(($page - 1) * self::ORDERS_PER_PAGE, self::ORDERS_PER_PAGE);

        $orders = [];
		foreach ($results as $result) {
			$product_total = $this->model_account_order->getTotalOrderProductsByOrderId($result['order_id']);
			$voucher_total = $this->model_account_order->getTotalOrderVouchersByOrderId($result['order_id']);

			$orders[] = [
				'order_id'   => $result['order_id'],
				'name'       => $result['firstname'] . ' ' . $result['lastname'],
				'status'     => $result['status'],
				'date_added' => date(self::DATE_FORMAT, strtotime($result['date_added'])),
				'products'   => $product_total + $voucher_total,
				'total'      => $this->currency->format($result['total'], $result['currency_code'], $result['currency_value']),
            ];
		}

        $this->data = [
            'orders'    => $orders,
            'next_page' => ceil($order_total / self::ORDERS_PER_PAGE) > $page ? ($page + 1) : 0,
        ];

        $this->send();
    }


    /**
     * Get order info endpoint.
     * Fields: order_id
     */
    public function getOrder()
    {
        $this->load->language('account/order');
        $this->load->model('account/order');

        $order_id = $this->request->get['order_id'] ?? 0;
        $order_info = $this->model_account_order->getOrder($order_id);

        if (!$order_info || !$this->customer->isLogged()) {
            $this->data['error'] = $this->language->get('text_error');
            $this->send();
            return;
        }

        $this->load->model('account/order');
        $this->load->model('tool/upload');

        // Products

        $products = [];
        foreach ($this->model_account_order->getOrderProducts($order_id) as $product) {
            $option_data = [];
            $options = $this->model_account_order->getOrderOptions($order_id, $product['order_product_id']);

            foreach ($options as $option) {
                if ($option['type'] != 'file') {
                    $value = $option['value'];
                } else {
                    $upload_info = $this->model_tool_upload->getUploadByCode($option['value']);

                    if ($upload_info) {
                        $value = $upload_info['name'];
                    } else {
                        $value = '';
                    }
                }

                $option_data[] = [
                    'name'  => $option['name'],
                    'value' => utf8_strlen($value) > 20 ? utf8_substr($value, 0, 20) . '..' : $value,
                ];
            }

            $products[] = [
                'name'     => $product['name'],
                'model'    => $product['model'],
                'option'   => $option_data,
                'quantity' => $product['quantity'],
                'price'    => $this->currency->format($product['price'] + ($this->config->get('config_tax') ? $product['tax'] : 0), $order_info['currency_code'], $order_info['currency_value']),
                'total'    => $this->currency->format($product['total'] + ($this->config->get('config_tax') ? ($product['tax'] * $product['quantity']) : 0), $order_info['currency_code'], $order_info['currency_value']),
            ];
        }

        // Totals

        $totals = [];

        foreach ($this->model_account_order->getOrderTotals($order_id) as $total) {
            $totals[] = [
                'title' => $total['title'],
                'text'  => $this->currency->format($total['value'], $order_info['currency_code'], $order_info['currency_value']),
            ];
        }

        // Histories

        $histories = [];
        foreach ($this->model_account_order->getOrderHistories($order_id) as $result) {
            $histories[] = [
                'date_added' => date(self::DATE_FORMAT, strtotime($result['date_added'])),
                'status'     => $result['status'],
                'comment'    => $result['notify'] ? $result['comment'] : '',
            ];
        }

        $this->data = [
            'order_info' => array_merge($order_info, [
                'date_added' => date(self::DATE_FORMAT, strtotime($order_info['date_added'])),
            ]),
            'products'   => $products,
            'totals'     => $totals,
            'histories'  => $histories,
        ];

        $this->send();
    }


    /**
     * Logout endpoint.
     */
    public function logout()
    {
        if (!$this->customer->isLogged()) {
            return;
        }

        $this->customer->logout();
        unset($this->session->data['shipping_address']);
        unset($this->session->data['shipping_method']);
        unset($this->session->data['shipping_methods']);
        unset($this->session->data['payment_address']);
        unset($this->session->data['payment_method']);
        unset($this->session->data['payment_methods']);
        unset($this->session->data['comment']);
        unset($this->session->data['order_id']);
        unset($this->session->data['coupon']);
        unset($this->session->data['reward']);
        unset($this->session->data['voucher']);
        unset($this->session->data['vouchers']);
        $this->send();
    }


    /**
     * Set current session shipping method endpoint.
     * Fields: payment_id
     */
    public function setShippingMethod()
    {
        if (empty($this->session->data['shipping_methods']) ||
            !isset($this->request->post['shipping_method'])) {
            $this->data['error'] = true;
            $this->send();
            return;
        }

        $shipping = explode('.', $this->request->post['shipping_method']);
        $method = $this->session->data['shipping_methods'][$shipping[0]]['quote'][$shipping[1]] ?? null;

        if (!isset($shipping[0]) || !isset($shipping[1]) || !$method) {
            $this->data['error'] = true;
            $this->send();
            return;
        }

        $this->session->data['shipping_method'] = $method;
        $this->send();
    }


    /**
     * Set current session payment method endpoint.
     * Fields: payment_id
     */
    public function setPaymentMethod()
    {
        if (empty($this->session->data['payment_methods'])) {
            $this->data['error'] = true;
            $this->send();
            return;
        }

        $this->session->data['payment_method'] = $this->session->data['payment_methods'][$this->request->post['payment_method'] ?? ''];
        $this->send();
    }


    /**
     * Forgotten password endpoint.
     * Fields: email
     */
    public function forgotten()
    {
        $this->load->language('account/forgotten');
        $this->load->model('account/customer');

        $this->data['error'] = false;

        if (!isset($this->request->post['email'])) {
			$this->data['error'] = $this->language->get('error_email');
		} elseif (!$this->model_account_customer->getTotalCustomersByEmail($this->request->post['email'])) {
			$this->data['error'] = $this->language->get('error_email');
		}

		$customer_info = $this->model_account_customer->getCustomerByEmail($this->request->post['email']);

		if ($customer_info && !$customer_info['status']) {
			$this->data['error'] = $this->language->get('error_approved');
        }

        if ($this->data['error']) {
            $this->send();
            return;
        }

        $this->model_account_customer->editCode($this->request->post['email'], token(40));
        $this->data['success'] = $this->language->get('text_success');
        $this->send();
    }


    /**
     * Change password endpoint.
     * Fields: password, confirm
     */
    public function changePassword()
    {
        $this->load->language('account/reset');

        $this->data['error'] = false;

        if (!$this->customer->isLogged()) {
            $this->data['error'] = $this->language->get('error_code');
        }

        if ((utf8_strlen(html_entity_decode($this->request->post['password'], ENT_QUOTES, 'UTF-8')) < 4) ||
            (utf8_strlen(html_entity_decode($this->request->post['password'], ENT_QUOTES, 'UTF-8')) > 40)) {
			$this->data['error'] = $this->language->get('error_password');
		}

		if ($this->request->post['confirm'] != $this->request->post['password']) {
			$this->data['error'] = $this->language->get('error_confirm');
        }

        if ($this->data['error']) {
            $this->send();
            return;
        }

        $this->load->model('account/customer');
        $this->model_account_customer->editPassword($this->customer->getEmail(), $this->request->post['password']);
        $this->data['success'] = $this->language->get('text_success');
        $this->send();
    }


    /**
     * Get wishlist endpoint.
     */
    public function getWishlist()
    {
        $this->load->model('account/wishlist');
        $this->load->model('mobile/index');
        $this->load->model('catalog/product');
        $this->data['products'] = [];

        foreach ($this->model_account_wishlist->getWishlist() as $product) {
            $data = $this->model_catalog_product->getProduct($product['product_id']);

            if ($data) {
                $this->data['products'][] = $this->model_mobile_index->getFormattedProduct($data);
            } else {
                $this->model_account_wishlist->deleteWishlist($product['product_id']);
            }
        }

        $this->send();
    }


    /**
     * Add product to wishlist endpoint.
     * Fields: product_id
     */
    public function addWishlist()
    {
        $product_id = $this->request->get['product_id'] ?? 0;

		$this->load->model('catalog/product');

		if ($this->model_catalog_product->getProduct($product_id)) {
			if ($this->customer->isLogged()) {
				$this->load->model('account/wishlist');
				$this->model_account_wishlist->addWishlist($product_id);
			} else {
				if (!isset($this->session->data['wishlist'])) {
					$this->session->data['wishlist'] = [];
				}

				$this->session->data['wishlist'][] = $product_id;
				$this->session->data['wishlist'] = array_unique($this->session->data['wishlist']);
            }

            $this->data['in_wishlist'] = true;
        } else {
            $this->data['in_wishlist'] = false;
        }

        $this->send();
    }


    /**
     * Remove product from wishlist endpoint.
     * Fields: product_id
     */
    public function deleteWishlist()
    {
        $this->load->model('account/wishlist');
        $this->model_account_wishlist->deleteWishlist($this->request->get['product_id'] ?? 0);
        $this->data['in_wishlist'] = false;
        $this->send();
    }


    /*
     * Get currencies endpoint.
     */
    public function getCurrencies()
    {
		$this->load->model('localisation/currency');

		$this->data['currencies'] = [];

		foreach ($this->model_localisation_currency->getCurrencies() as $result) {
			if ($result['status']) {
				$this->data['currencies'][] = [
					'title'        => $result['title'],
					'code'         => $result['code'],
					'symbol_left'  => $result['symbol_left'],
                    'symbol_right' => $result['symbol_right'],
                    'checked'      => $result['code'] == $this->session->data['currency'],
                ];
			}
        }

        $this->send();
    }


    /*
     * Set currency endpoint.
     * Fields: currency_code
     */
    public function setCurrency()
    {
        $this->session->data['currency'] = $this->request->get['currency_code'] ?? $this->currency->get('config_currency');
        $this->getCurrencies();
    }


    private function validateAddress()
    {
        $errors = [];

        if ((utf8_strlen(trim($this->request->post['firstname'])) < 1) ||
            (utf8_strlen(trim($this->request->post['firstname'])) > 32)) {
            $errors['firstname'] = $this->language->get('error_firstname');
        }

        if ((utf8_strlen(trim($this->request->post['lastname'])) < 1) ||
            (utf8_strlen(trim($this->request->post['lastname'])) > 32)) {
            $errors['lastname'] = $this->language->get('error_lastname');
        }

        if ((utf8_strlen(trim($this->request->post['address_1'])) < 3) ||
            (utf8_strlen(trim($this->request->post['address_1'])) > 128)) {
            $errors['address_1'] = $this->language->get('error_address_1');
        }

        if ((utf8_strlen(trim($this->request->post['city'])) < 2) ||
            (utf8_strlen(trim($this->request->post['city'])) > 128)) {
            $errors['city'] = $this->language->get('error_city');
        }

        $this->load->model('localisation/country');
        $country_info = $this->model_localisation_country->getCountry(self::DEFAULT_COUNTRY_ID);

        if ($country_info && $country_info['postcode_required'] &&
            (utf8_strlen(trim($this->request->post['postcode'])) < 2 ||
             utf8_strlen(trim($this->request->post['postcode'])) > 10)) {
			$errors['postcode'] = $this->language->get('error_postcode');
        }

        if (empty($this->request->post['zone_id']) || !is_numeric($this->request->post['zone_id'])) {
			$errors['zone'] = $this->language->get('error_zone');
        }

        return $errors;
    }

}
