<?php

class ControllerMobileCheckout extends MobileController
{

    const IMAGE_WIDTH = 200;

    const IMAGE_HEIGHT = 200;


    /**
     * Get cart endpoint.
     */
    public function getCart()
    {
        $this->data = [
            'products' => [],
            'totals'   => [],
            'weight'   => '',
            'error'    => '',
        ];

        $products = $this->cart->getProducts();

        if (empty($products)) {
            $this->send();
            return;
        }

        if ($this->request->get['clear_shipping_method'] ?? false) {
            unset($this->session->data['shipping_method']);
        }

        $this->load->language('checkout/cart');
        $this->load->model('tool/image');
        $this->load->model('account/customerpartner');

        $this->data['weight'] = $this->config->get('config_cart_weight') ?
            $this->weight->format($this->cart->getWeight(), $this->config->get('config_weight_class_id'), $this->language->get('decimal_point'), $this->language->get('thousand_point')) :
            '';

        if (!$this->cart->hasStock() && (!$this->config->get('config_stock_checkout') || $this->config->get('config_stock_warning'))) {
            $this->data['error'] = $this->language->get('error_stock');
        }

        // Products
        foreach ($products as $product) {
            $product_total = 0;

            foreach ($products as $product_2) {
                if ($product_2['product_id'] == $product['product_id']) {
                    $product_total += $product_2['quantity'];
                }
            }

            if ($product['minimum'] > $product_total) {
                $this->data['error'] = sprintf($this->language->get('error_minimum'), $product['name'], $product['minimum']);
            }

            if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
                $unit_price = $this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax'));

                $price = $this->currency->format($unit_price, $this->session->data['currency']);
                $total = $this->currency->format($unit_price * $product['quantity'], $this->session->data['currency']);
            } else {
                $price = false;
                $total = false;
            }

            $seller = $this->model_account_customerpartner->getSellerByProductId($product['product_id']);

            $this->data['products'][] = [
                'cart_id'    => $product['cart_id'],
                'product_id' => $product['product_id'],
                'name'       => $product['name'],
                'quantity'   => $product['quantity'],
                'stock'      => $product['stock'],
                'minimum'    => $product['minimum'],
                'thumb'      => $this->model_tool_image->resize($product['image'] ?? 'placeholder.png', self::IMAGE_WIDTH, self::IMAGE_HEIGHT),
                'seller'     => $seller['companyname'] ?? '',
                'price'      => $price,
                'total'      => $total,
            ];
        }

        // Totals
        $this->load->model('setting/extension');

        $totals = [];
        $taxes = $this->cart->getTaxes();
        $total = 0;

        $total_data = [
            'totals' => &$totals,
            'taxes'  => &$taxes,
            'total'  => &$total,
        ];

        if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
            $sort_order = [];

            $results = $this->model_setting_extension->getExtensions('total');
            foreach ($results as $key => $value) {
                $sort_order[$key] = $this->config->get('total_' . $value['code'] . '_sort_order');
            }

            array_multisort($sort_order, SORT_ASC, $results);

            foreach ($results as $result) {
                if ($this->config->get('total_' . $result['code'] . '_status')) {
                    $this->load->model('extension/total/' . $result['code']);
                    $this->{'model_extension_total_' . $result['code']}->getTotal($total_data);
                }
            }

            $sort_order = [];
            foreach ($totals as $key => $value) {
                $sort_order[$key] = $value['sort_order'];
            }

            array_multisort($sort_order, SORT_ASC, $totals);
        }

        $this->data['totals'] = [];
        foreach ($totals as $total) {
            $this->data['totals'][] = [
                'title' => $total['title'],
                'text'  => $this->currency->format($total['value'], $this->session->data['currency']),
            ];
        }

        $this->send();
    }


    /**
     * Update products cart quantity endpoint.
     * Fields: quantity
     */
    public function changeCartQuantity()
    {
        foreach (($this->request->post['quantity'] ?? []) as $key => $value) {
            $this->cart->update($key, $value);
        }

        $this->getCart();
    }


    /**
     * Delete product from cart.
     * Fields: cart_id
     */
    public function deleteProduct()
    {
        $this->load->language('checkout/cart');

		if (isset($this->request->post['cart_id'])) {
			$this->cart->remove($this->request->post['cart_id']);
			unset($this->session->data['vouchers'][$this->request->post['cart_id']]);
        }

        $this->getCart();
    }


    /**
     * Get shipping methods endpoint.
     */
    public function getShippingMethods()
    {
        $this->setShippingMethods();
        $this->data['methods'] = array_values($this->session->data['shipping_methods']);
        $this->send();
    }


    /**
     * Get payment methods endpoint.
     */
    public function getPaymentMethods()
    {
        $this->setPaymentMethods();
        $this->data['methods'] = array_values($this->session->data['payment_methods']);
        $this->send();
    }


    /**
     * Get totals list endpoint.
     */
    public function getTotals()
    {
        $totals = [];
        $taxes = $this->cart->getTaxes();
        $total = 0;

        $total_data = [
            'totals' => &$totals,
            'taxes'  => &$taxes,
            'total'  => &$total,
        ];

        $this->load->model('setting/extension');

        $sort_order = [];

        $results = $this->model_setting_extension->getExtensions('total');

        foreach ($results as $key => $value) {
            $sort_order[$key] = $this->config->get('total_' . $value['code'] . '_sort_order');
        }

        array_multisort($sort_order, SORT_ASC, $results);

        foreach ($results as $result) {
            if ($this->config->get('total_' . $result['code'] . '_status')) {
                $this->load->model('extension/total/' . $result['code']);

                // We have to put the totals in an array so that they pass by reference.
                $this->{'model_extension_total_' . $result['code']}->getTotal($total_data);
            }
        }

        $sort_order = [];

        foreach ($totals as $key => $value) {
            $sort_order[$key] = $value['sort_order'];
        }

        array_multisort($sort_order, SORT_ASC, $totals);

        $this->data['totals'] = [];

        foreach ($totals as $total) {
            $this->data['totals'][] = [
                'title' => $total['title'],
                'text'  => $this->currency->format($total['value'], $this->session->data['currency']),
            ];
        }

        $this->send();
    }



    /**
     * Endpoint to initialize the shipping address, payment address, shipping methods and payment methods
     * based on the given address id.
     * Fields: address_id
     */
    public function init()
    {
        $this->load->model('account/address');
        $address = $this->model_account_address->getAddress($this->request->get['address_id'] ?? 0);

        if (!$address) {
            $this->data['error'] = true;
            $this->send();
            return;
        }

        // Reset session data
        unset($this->session->data['shipping_address']);
        unset($this->session->data['shipping_method']);
        unset($this->session->data['shipping_methods']);
        unset($this->session->data['payment_address']);
        unset($this->session->data['payment_method']);
        unset($this->session->data['payment_methods']);

        $this->load->model('localisation/country');
        $this->load->model('localisation/zone');
        $country_info = $this->model_localisation_country->getCountry($address['country_id']);
        $zone_info = $this->model_localisation_zone->getZone($address['zone_id']);

        $data = [
            'firstname'      => $address['firstname'],
            'lastname'       => $address['lastname'],
            'company'        => $address['company'],
            'address_1'      => $address['address_1'],
            'address_2'      => $address['address_2'],
            'postcode'       => $address['postcode'],
            'city'           => $address['city'],
            'zone_id'        => $address['zone_id'],
            'zone'           => $zone_info ? $zone_info['name'] : '',
            'zone_code'      => $zone_info ? $zone_info['code'] : '',
            'country_id'     => $address['country_id'],
            'country'        => $country_info ? $country_info['name'] : '',
            'iso_code_2'     => $country_info ? $country_info['iso_code_2'] : '',
            'iso_code_3'     => $country_info ? $country_info['iso_code_3'] : '',
            'address_format' => $country_info ? $country_info['address_format'] : '',
            'custom_field'   => $address['custom_field'] ?? [],
        ];

        $this->session->data['shipping_address'] = $data;
        $this->session->data['payment_address'] = $data;
        $this->setShippingMethods();
        $this->setPaymentMethods();

        $this->send();
    }


    /**
     * Get current session address endpoint.
     */
    public function getAddress()
    {
        $this->data['address'] = $this->session->data['shipping_address'];
        $this->send();
    }


    /**
     * Finish order endpoint.
     * Fields: comment
     */
    public function confirm()
    {
        $error = false;

        if ($this->cart->hasShipping()) {
            if (!isset($this->session->data['shipping_address']) ||
                !isset($this->session->data['shipping_method'])) {
				$error = 'shipping';
			}
		} else {
			unset($this->session->data['shipping_address']);
			unset($this->session->data['shipping_method']);
			unset($this->session->data['shipping_methods']);
        }

        if (!$this->customer->isLogged()) {
            $error = 'logged';
        }

        if (!isset($this->session->data['payment_address']) ||
            !isset($this->session->data['payment_method']) ||
            (!$this->cart->hasProducts() && empty($this->session->data['vouchers'])) ||
            (!$this->cart->hasStock() && !$this->config->get('config_stock_checkout'))) {
			$error = 'payment';
		}

		// Validate minimum quantity requirements.
		$products = $this->cart->getProducts();

		foreach ($products as $product) {
			$product_total = 0;

			foreach ($products as $product_2) {
				if ($product_2['product_id'] == $product['product_id']) {
					$product_total += $product_2['quantity'];
				}
			}

			if ($product['minimum'] > $product_total) {
				$error = 'quantity';
				break;
			}
        }

        if ($error) {
            $this->data['error'] = $error;
            $this->send();
            return;
        }


        $order_data = [];
        $totals = [];
        $taxes = $this->cart->getTaxes();
        $total = 0;

        // Because __call can not keep var references so we put them into an array.
        $total_data = [
            'totals' => &$totals,
            'taxes'  => &$taxes,
            'total'  => &$total,
        ];

        $this->load->model('setting/extension');

        $sort_order = [];
        $results = $this->model_setting_extension->getExtensions('total');

        foreach ($results as $key => $value) {
            $sort_order[$key] = $this->config->get('total_' . $value['code'] . '_sort_order');
        }

        array_multisort($sort_order, SORT_ASC, $results);

        foreach ($results as $result) {
            if ($this->config->get('total_' . $result['code'] . '_status')) {
                $this->load->model('extension/total/' . $result['code']);

                // We have to put the totals in an array so that they pass by reference.
                $this->{'model_extension_total_' . $result['code']}->getTotal($total_data);
            }
        }

        $sort_order = [];

        foreach ($totals as $key => $value) {
            $sort_order[$key] = $value['sort_order'];
        }

        array_multisort($sort_order, SORT_ASC, $totals);

        $order_data['totals'] = $totals;

        $this->load->language('checkout/checkout');

        $order_data['invoice_prefix'] = $this->config->get('config_invoice_prefix');
        $order_data['store_id'] = $this->config->get('config_store_id');
        $order_data['store_name'] = $this->config->get('config_name');

        if ($order_data['store_id']) {
            $order_data['store_url'] = $this->config->get('config_url');
        } else {
            if ($this->request->server['HTTPS']) {
                $order_data['store_url'] = HTTPS_SERVER;
            } else {
                $order_data['store_url'] = HTTP_SERVER;
            }
        }

        $this->load->model('account/customer');

        $customer_info = $this->model_account_customer->getCustomer($this->customer->getId());

        $order_data['customer_id'] = $this->customer->getId();
        $order_data['customer_group_id'] = $customer_info['customer_group_id'];
        $order_data['firstname'] = $customer_info['firstname'];
        $order_data['lastname'] = $customer_info['lastname'];
        $order_data['email'] = $customer_info['email'];
        $order_data['telephone'] = $customer_info['telephone'];
        $order_data['custom_field'] = json_decode($customer_info['custom_field'], true);

        $order_data['payment_firstname'] = $this->session->data['payment_address']['firstname'];
        $order_data['payment_lastname'] = $this->session->data['payment_address']['lastname'];
        $order_data['payment_company'] = $this->session->data['payment_address']['company'];
        $order_data['payment_address_1'] = $this->session->data['payment_address']['address_1'];
        $order_data['payment_address_2'] = $this->session->data['payment_address']['address_2'];
        $order_data['payment_city'] = $this->session->data['payment_address']['city'];
        $order_data['payment_postcode'] = $this->session->data['payment_address']['postcode'];
        $order_data['payment_zone'] = $this->session->data['payment_address']['zone'];
        $order_data['payment_zone_id'] = $this->session->data['payment_address']['zone_id'];
        $order_data['payment_country'] = $this->session->data['payment_address']['country'];
        $order_data['payment_country_id'] = $this->session->data['payment_address']['country_id'];
        $order_data['payment_address_format'] = $this->session->data['payment_address']['address_format'];
        $order_data['payment_custom_field'] = $this->session->data['payment_address']['custom_field'] ?? [];
        $order_data['payment_method'] = $this->session->data['payment_method']['title'] ?? '';
        $order_data['payment_code'] = $this->session->data['payment_method']['code'] ?? '';

        if ($this->cart->hasShipping()) {
            $order_data['shipping_firstname'] = $this->session->data['shipping_address']['firstname'];
            $order_data['shipping_lastname'] = $this->session->data['shipping_address']['lastname'];
            $order_data['shipping_company'] = $this->session->data['shipping_address']['company'];
            $order_data['shipping_address_1'] = $this->session->data['shipping_address']['address_1'];
            $order_data['shipping_address_2'] = $this->session->data['shipping_address']['address_2'];
            $order_data['shipping_city'] = $this->session->data['shipping_address']['city'];
            $order_data['shipping_postcode'] = $this->session->data['shipping_address']['postcode'];
            $order_data['shipping_zone'] = $this->session->data['shipping_address']['zone'];
            $order_data['shipping_zone_id'] = $this->session->data['shipping_address']['zone_id'];
            $order_data['shipping_country'] = $this->session->data['shipping_address']['country'];
            $order_data['shipping_country_id'] = $this->session->data['shipping_address']['country_id'];
            $order_data['shipping_address_format'] = $this->session->data['shipping_address']['address_format'];
            $order_data['shipping_custom_field'] = $this->session->data['shipping_address']['custom_field'] ?? [];
            $order_data['shipping_method'] = $this->session->data['shipping_method']['title'] ?? '';
            $order_data['shipping_code'] = $this->session->data['shipping_method']['code'] ?? '';
        } else {
            $order_data['shipping_firstname'] = '';
            $order_data['shipping_lastname'] = '';
            $order_data['shipping_company'] = '';
            $order_data['shipping_address_1'] = '';
            $order_data['shipping_address_2'] = '';
            $order_data['shipping_city'] = '';
            $order_data['shipping_postcode'] = '';
            $order_data['shipping_zone'] = '';
            $order_data['shipping_zone_id'] = '';
            $order_data['shipping_country'] = '';
            $order_data['shipping_country_id'] = '';
            $order_data['shipping_address_format'] = '';
            $order_data['shipping_custom_field'] = [];
            $order_data['shipping_method'] = '';
            $order_data['shipping_code'] = '';
        }

        $order_data['products'] = [];

        foreach ($this->cart->getProducts() as $product) {
            $option_data = [];

            foreach ($product['option'] as $option) {
                $option_data[] = [
                    'product_option_id'       => $option['product_option_id'],
                    'product_option_value_id' => $option['product_option_value_id'],
                    'option_id'               => $option['option_id'],
                    'option_value_id'         => $option['option_value_id'],
                    'name'                    => $option['name'],
                    'value'                   => $option['value'],
                    'type'                    => $option['type'],
                ];
            }

            $order_data['products'][] = [
                'product_id' => $product['product_id'],
                'name'       => $product['name'],
                'model'      => $product['model'],
                'option'     => $option_data,
                'download'   => $product['download'],
                'quantity'   => $product['quantity'],
                'subtract'   => $product['subtract'],
                'price'      => $product['price'],
                'total'      => $product['total'],
                'tax'        => $this->tax->getTax($product['price'], $product['tax_class_id']),
                'reward'     => $product['reward'],
            ];
        }

        $order_data['comment'] = $this->request->post['comment'] ?? '';
        $order_data['total'] = $total_data['total'];

        if (isset($this->request->cookie['tracking'])) {
            $order_data['tracking'] = $this->request->cookie['tracking'];

            $subtotal = $this->cart->getSubTotal();

            // Affiliate
            $affiliate_info = $this->model_account_customer->getAffiliateByTracking($this->request->cookie['tracking']);

            if ($affiliate_info) {
                $order_data['affiliate_id'] = $affiliate_info['customer_id'];
                $order_data['commission'] = ($subtotal / 100) * $affiliate_info['commission'];
            } else {
                $order_data['affiliate_id'] = 0;
                $order_data['commission'] = 0;
            }

            // Marketing
            $this->load->model('checkout/marketing');

            $marketing_info = $this->model_checkout_marketing->getMarketingByCode($this->request->cookie['tracking']);
            $order_data['marketing_id'] = $marketing_info ? $marketing_info['marketing_id'] : 0;
        } else {
            $order_data['affiliate_id'] = 0;
            $order_data['commission'] = 0;
            $order_data['marketing_id'] = 0;
            $order_data['tracking'] = '';
        }

        $order_data['language_id'] = $this->config->get('config_language_id');
        $order_data['currency_id'] = $this->currency->getId($this->session->data['currency']);
        $order_data['currency_code'] = $this->session->data['currency'];
        $order_data['currency_value'] = $this->currency->getValue($this->session->data['currency']);
        $order_data['ip'] = $this->request->server['REMOTE_ADDR'];

        if (!empty($this->request->server['HTTP_X_FORWARDED_FOR'])) {
            $order_data['forwarded_ip'] = $this->request->server['HTTP_X_FORWARDED_FOR'];
        } elseif (!empty($this->request->server['HTTP_CLIENT_IP'])) {
            $order_data['forwarded_ip'] = $this->request->server['HTTP_CLIENT_IP'];
        } else {
            $order_data['forwarded_ip'] = '';
        }

        $order_data['user_agent'] = $this->request->server['HTTP_USER_AGENT'] ?? '';
        $order_data['accept_language'] = $this->request->server['HTTP_ACCEPT_LANGUAGE'] ?? '';

        $this->load->model('checkout/order');

        $this->session->data['order_id'] = $this->model_checkout_order->addOrder($order_data);
        $this->model_checkout_order->addOrderHistory($this->session->data['order_id'], $this->config->get('config_order_status_id'));
        $this->data['order_id'] = $this->session->data['order_id'];

        if (isset($this->session->data['order_id'])) {
			$this->cart->clear();
			unset($this->session->data['shipping_method']);
			unset($this->session->data['shipping_methods']);
			unset($this->session->data['payment_method']);
			unset($this->session->data['payment_methods']);
			unset($this->session->data['guest']);
			unset($this->session->data['comment']);
			unset($this->session->data['order_id']);
			unset($this->session->data['coupon']);
			unset($this->session->data['reward']);
			unset($this->session->data['voucher']);
			unset($this->session->data['vouchers']);
			unset($this->session->data['totals']);
        }

        $this->send();
    }


    private function setPaymentMethods()
    {
        // Totals
        $totals = [];
        $taxes = $this->cart->getTaxes();
        $total = 0;

        // Because __call can not keep var references so we put them into an array.
        $total_data = [
            'totals' => &$totals,
            'taxes'  => &$taxes,
            'total'  => &$total,
        ];

        $this->load->model('setting/extension');

        $sort_order = [];

        $results = $this->model_setting_extension->getExtensions('total');

        foreach ($results as $key => $value) {
            $sort_order[$key] = $this->config->get('total_' . $value['code'] . '_sort_order');
        }

        array_multisort($sort_order, SORT_ASC, $results);

        foreach ($results as $result) {
            if ($this->config->get('total_' . $result['code'] . '_status')) {
                $this->load->model('extension/total/' . $result['code']);

                // We have to put the totals in an array so that they pass by reference.
                $this->{'model_extension_total_' . $result['code']}->getTotal($total_data);
            }
        }

        $methods = [];

        $this->load->model('setting/extension');

        $recurring = $this->cart->hasRecurringProducts();

        foreach ($this->model_setting_extension->getExtensions('payment') as $result) {
            if ($this->config->get('payment_' . $result['code'] . '_status')) {
                $this->load->model('extension/payment/' . $result['code']);

                $method = $this->{'model_extension_payment_' . $result['code']}->getMethod($this->session->data['payment_address'], $total);

                if ($method) {
                    if ($recurring) {
                        if (property_exists($this->{'model_extension_payment_' . $result['code']}, 'recurringPayments') && $this->{'model_extension_payment_' . $result['code']}->recurringPayments()) {
                            $methods[$result['code']] = $method;
                        }
                    } else {
                        $methods[$result['code']] = $method;
                    }
                }
            }
        }

        $sort_order = [];
        foreach ($methods as $key => $value) {
            $sort_order[$key] = $value['sort_order'];
        }

        array_multisort($sort_order, SORT_ASC, $methods);

        $this->session->data['payment_methods'] = $methods;
    }


    private function setShippingMethods()
    {
        $this->load->model('setting/extension');

        $this->data['methods'] = [];

        foreach ($this->model_setting_extension->getExtensions('shipping') as $result) {
            if ($this->config->get('shipping_' . $result['code'] . '_status')) {
                $this->load->model('extension/shipping/' . $result['code']);

                $quote = $this->{'model_extension_shipping_' . $result['code']}->getQuote($this->session->data['shipping_address'] ?? '');

                if ($quote) {
                    $this->data['methods'][$result['code']] = [
                        'title'      => $quote['title'],
                        'quote'      => $quote['quote'],
                        'sort_order' => $quote['sort_order'],
                        'error'      => $quote['error'],
                    ];
                }
            }
        }

        $sort_order = [];

        foreach ($this->data['methods'] as $key => $value) {
            $sort_order[$key] = $value['sort_order'];
        }

        array_multisort($sort_order, SORT_ASC, $this->data['methods']);

        $this->session->data['shipping_methods'] = $this->data['methods'];
    }

}
