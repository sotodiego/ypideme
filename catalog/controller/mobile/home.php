<?php

class ControllerMobileHome extends MobileController
{


    /**
     * Get home modules endpoint.
     */
    public function get()
    {
        $this->load->model('mobile/index');
        $this->data = [
            'modules' => []
        ];

        foreach ($this->model_mobile_index->getHomeModules() as $val) {
            $module = (array) json_decode($val['data']);
            switch ($val['type']) {
                case 0: $content = $this->getBannersData($module['banner']); break;
                case 1: $content = $this->getProductsData($module); break;
                default: $content = [];
            }

            $this->data['modules'][] = [
                'name'    => $val['name'],
                'type'    => $val['type'],
                'content' => $content,
            ];
        }

        $this->send();
    }


    private function getBannersData($banner_id)
    {
        $this->load->model('design/banner');
        $this->load->model('tool/image');

        $banners = $this->model_design_banner->getBanner($banner_id);
        foreach ($banners as $key => $val) {
            $banners[$key]['image'] = $this->model_tool_image->resize($val['image'], 300, 300);
        }

        return $banners;
    }


    private function getProductsData($module)
    {
        $this->load->model('mobile/index');

        switch ($module['list']) {
            case 'most_viewed':
                $this->load->model('journal3/filter');

                foreach ($this->model_journal3_filter->getProducts([
                    'limit' => $module['limit'],
                    'sort'  => 'fp.viewed',
                    'order' => 'DESC',
                ]) as $product) {
                    $products[] = $this->model_mobile_index->getFormattedProduct($product);
                }
                break;
            case 'custom':
                $this->load->model('catalog/product');

                foreach ($module['products'] as $product_id) {
                    $data = $this->model_catalog_product->getProduct($product_id);
                    $products[] = $this->model_mobile_index->getFormattedProduct($data);
                }
                break;
            default: $products = [];
        }

        return array_merge($module, [
            'products' => $products,
        ]);
    }
}
