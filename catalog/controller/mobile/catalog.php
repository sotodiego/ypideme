<?php

class ControllerMobileCatalog extends MobileController
{


    /**
     * Search product endpoint.
     * Fields: search, category_id, page
     */
    public function search()
    {
        // Filters
        $limit = $this->config->get('theme_' . $this->config->get('config_theme') . '_product_limit');
        $page = isset($this->request->get['page']) ? max(1, (int)$this->request->get['page']) : 1;
        $sort = explode('-', $this->request->get['sort_by'] ?? '');

        $filter = [
            'filter_name'         => $this->request->get['search'] ?? '',
            'filter_tag'          => '',
            'filter_description'  => '',
            'filter_category_id'  => $this->request->get['category_id'] ?? 0,
            'filter_sub_category' => '',
            'sort'                => $sort[0] ?? 'p.sort_order',
            'order'               => $sort[1] ?? 'ASC',
            'start'               => ($page - 1) * $limit,
            'limit'               => $limit,
        ];

        $product_total = defined('JOURNAL3_ACTIVE') ?
            $this->model_journal3_filter->getTotalProducts() :
            $this->model_catalog_product->getTotalProducts($filter);

        $results = defined('JOURNAL3_ACTIVE') ?
            $this->model_journal3_filter->getProducts($filter) :
            $this->model_catalog_product->getProducts($filter);

        // Format data
        $this->load->model('journal3/image');
        $this->load->model('tool/image');
        $this->load->model('account/customerpartner');
        $this->load->model('mobile/index');
        $products = [];

        foreach ($results as $result) {
            $seller = $this->model_account_customerpartner->getSellerByProductId($result['product_id']);
            $product = $this->model_mobile_index->getFormattedProduct($result);
            $product['seller'] = $seller['companyname'] ?? '';
            $products[] = $product;
        }

        $this->data = [
            'products'  => $products,
            'next_page' => ceil($product_total / $limit) > $page ? ($page + 1) : 0,
        ];

        $this->send();
    }


    /**
     * Get product endpoint.
     * Fields: product_id
     */
    public function getProduct()
    {
        $this->load->language('product/product');
        $this->load->model('catalog/product');
        $this->load->model('catalog/review');
        $this->load->model('account/customerpartner');
        $this->load->model('mobile/index');
        $this->load->model('account/wishlist');

        $product_id = $this->request->get['product_id'] ?? 0;
        $product = $this->model_catalog_product->getProduct($product_id);

        if (!$product) {
            $this->data['error'] = $this->language->get('');
            $this->send();
            return;
        }

        // Related
        $related = [];
        foreach ($this->model_catalog_product->getProductRelated($product_id) as $product) {
            $related[] = $this->model_mobile_index->getFormattedProduct($product);
        }

        // Reviews
        $reviews = [];
		foreach ($this->model_catalog_review->getReviewsByProductId($product_id, 0, PHP_INT_MAX) as $result) {
			$reviews[] = [
				'author'     => $result['author'],
				'text'       => nl2br($result['text']),
				'rating'     => (int) $result['rating'],
				'date_added' => date('d/m/Y', strtotime($result['date_added']))
            ];
        }

        $this->data = [
            'product'  => $this->model_mobile_index->getFormattedProduct($product),
            'seller'   => $this->model_mobile_index->getFormattedSeller($this->model_account_customerpartner->getSellerByProductId($product_id)),
            'wishlist' => in_array($product_id, array_column($this->model_account_wishlist->getWishlist(), 'product_id')),
            'related'  => $related,
            'reviews'  => $reviews,
        ];

        $this->send();
    }


    /**
     * Add product to cart endpoint.
     * Fields: product_id, option, recurring_id
     */
    public function addProduct()
    {
        $this->load->language('checkout/cart');
        $this->load->model('catalog/product');

        $product_id = $this->request->post['product_id'] ?? 0;
        $quantity = isset($this->request->post['quantity']) ? (int)$this->request->post['quantity'] : 0;
        $options = isset($this->request->post['option']) ? array_filter($this->request->post['option']) : [];
        $recurring_id = $this->request->post['recurring_id'] ?? 0;

        $product_info = $this->model_catalog_product->getProduct($product_id);

        if (!$product_info) {
            $this->data['error'] = $this->language->get('error_exists');
            $this->send();
            return;
        }

        // Options
        foreach ($this->model_catalog_product->getProductOptions($product_id) as $option) {
            if ($option['required'] && empty($option[$option['product_option_id']])) {
                $this->data['error'] = sprintf($this->language->get('error_required'), $option['name']);
                $this->send();
                return;
            }
        }

        // Recurrings
        $recurrings = $this->model_catalog_product->getProfiles($product_id);

        if ($recurrings) {
            $recurring_ids = [];
            foreach ($recurrings as $recurring) {
                $recurring_ids[] = $recurring['recurring_id'];
            }

            if (!in_array($recurring_id, $recurring_ids)) {
                $this->data['error'] = $this->language->get('error_recurring_required');
                $this->send();
                return;
            }
        }

        $this->cart->add($product_id, $quantity, $options, $recurring_id);
        $this->send();
    }


    /**
     * Add product review endpoint.
     * Fields: product_id, name, text, rating
     */
    public function addReview()
    {
        $this->load->language('product/product');

        if ((utf8_strlen($this->request->post['name']) < 3) ||
            (utf8_strlen($this->request->post['name']) > 25)) {
            $this->data['error'] = $this->language->get('error_name');
        }

        if ((utf8_strlen($this->request->post['text']) < 25) ||
            (utf8_strlen($this->request->post['text']) > 1000)) {
            $this->data['error'] = $this->language->get('error_text');
        }

        if ($this->request->post['rating'] <= 0 ||
            $this->request->post['rating'] > 5) {
            $this->data['error'] = $this->language->get('error_rating');
        }

        if (isset($this->data['error'])) {
            $this->send();
            return;
        }

        $this->load->model('catalog/review');
        $this->model_catalog_review->addReview($this->request->get['product_id'], $this->request->post);
        $this->send();
    }


    /**
     * Get categories endpoint.
     */
    public function getCategories()
    {
        $this->load->model('journal3/category');

        if ($this->journal3->settings->get('searchStyleSearchCategoriesType') === 'all') {
            $this->data['categories'] = $this->model_journal3_category->getSubcategories(0);
            $this->send();
            return;
        }

        $categories = $this->model_journal3_category->getCategories(0, 0, $this->journal3->settings->get('searchStyleSearchCategoriesType') === 'top_only');

        $this->data['categories'] = [];

        foreach ($categories as $category) {
            $this->data['categories'][] =[
                'category_id' => $category['category_id'],
                'title'       => $category['name'],
                'items'       => [],
            ];
        }

        $this->send();
    }

}
