<?php

$_['heading_title']     = 'APP';
$_['title_modules']     = 'Modules';
$_['title_edit_create'] = 'Edit/Create';
$_['label_name']        = 'Name';
$_['label_type']        = 'Type';
$_['label_sort_order']  = 'Order';
$_['label_status']      = 'Status';
$_['label_action']      = 'Action';
$_['label_date_added']  = 'Date added';
$_['label_banner']      = 'Banner';
$_['label_product']     = 'Product';
$_['label_products']    = 'Products';
$_['label_list']        = 'Product list';
$_['label_categories']  = 'Category list';
$_['label_orientation'] = 'Orientation';
$_['label_limit']       = 'Limit';

$_['text_list_custom']      = 'Custom (below list)';
$_['text_list_specials']    = 'Specials';
$_['text_list_most_viewed'] = 'Most see';
$_['text_confirm_delete']   = 'Are you sure about deleting the module?';

$_['text_banner']       = 'Banner';
$_['text_categories']   = 'Categories';
$_['text_horizontal']   = 'Horizontal';
$_['text_vertical']     = 'Vertical';

$_['text_add_banner']    = 'Add banner';
$_['text_submit']        = 'Save';

$_['error_name'] = 'Name cannot be empty';
$_['success'] = 'The changes has been successfully made';
