<?php

class ModelMobileMain extends Model
{

    public function getHomeModules()
    {
        try {
            $this->db->query("SELECT * FROM " . DB_PREFIX . "mobile_home");
        } catch (Exception $e) {
            $this->db->query("CREATE TABLE `oc_mobile_home` (
                `mobile_home_id` INT NOT NULL AUTO_INCREMENT,
                `name` VARCHAR(255) NOT NULL,
                `type` TINYINT NOT NULL,
                `data` TEXT NOT NULL,
                `sort_order` INT NOT NULL,
                `status` BOOLEAN NOT NULL,
                `date_added` DATE NOT NULL,
                PRIMARY KEY (`mobile_home_id`)
            )");
        }

        return $this->db->query("SELECT * FROM " . DB_PREFIX . "mobile_home
            ORDER BY sort_order")->rows;
	}


    public function getHomeModule(int $module_id = 0)
    {
        return $this->db->query("SELECT * FROM " . DB_PREFIX . "mobile_home
            WHERE mobile_home_id = '$module_id'")->row;
    }


    public function getContentArr($data, $type)
    {
        switch ($type) {
            case 0:
                $content = [
                    'banner' => $this->db->escape($data['banner']),
                ];

                break;
            case 1:
                $content = [
                    'list'     => $data['list'],
                    'limit'    => $data['limit'],
                    'products' => [],
                ];

                foreach ($data['products'] as $product_id) {
                    $content['products'][] = (int) $product_id;
                }

                break;
        }

        return $content;
    }


    public function editHomeModule(int $module_id, $data)
    {
        $this->db->query("UPDATE " . DB_PREFIX . "mobile_home SET
            name       = '" . $this->db->escape($data['name']) . "',
            type       = '" . (int) $data['type'] . "',
            data       = '" . json_encode($this->getContentArr($data, $data['type'])) . "',
            sort_order = '" . (int) $data['sort_order'] . "',
            status     = '" . (bool) $data['status'] . "'
            WHERE mobile_home_id = '$module_id'");
    }


    public function addHomeModule(array $data = [])
    {
        $this->db->query("INSERT INTO " . DB_PREFIX . "mobile_home SET
            name       = '" . $this->db->escape($data['name']) . "',
            type       = '" . (int) $data['type'] . "',
            data       = '" . json_encode($this->getContentArr($data, $data['type'])) . "',
            sort_order = '" . (int) $data['sort_order'] . "',
            status     = '" . (bool) $data['status'] . "',
            date_added = NOW()");
    }


    public function deleteHomeModule(int $module_id)
    {
        $this->db->query("DELETE FROM " . DB_PREFIX . "mobile_home
            WHERE mobile_home_id = '$module_id'");
    }

}
