<?php

class ControllerMobileMain extends Controller
{

    /**
     * Home modules list.
     */
    public function index()
    {
        $this->load->model('mobile/main');
        $this->load->language('mobile/main');
        $this->document->setTitle($this->language->get('heading_title'));

        $data = [
            'breadcrumbs'  => [
                [
                    'text' => $this->language->get('text_home'),
                    'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true),
                ],
                [
                    'text' => $this->language->get('heading_title'),
                    'href' => $this->url->link('mobile/main', 'user_token=' . $this->session->data['user_token'], true),
                ],
            ],
            'user_token'   => $this->session->data['user_token'],
            'header'       => $this->load->controller('common/header'),
            'column_left'  => $this->load->controller('common/column_left'),
            'footer'       => $this->load->controller('common/footer'),
            'home_modules' => $this->model_mobile_main->getHomeModules(),
            'edit_href'    => $this->url->link('mobile/main/edit', 'user_token=' . $this->session->data['user_token'], true),
            'success'      => false,
        ];

        if ($this->session->data['success'] ?? false) {
            $data['success'] = $this->language->get('success');
            $this->session->data['success'] = false;
        }

		$this->response->setOutput($this->load->view('mobile/main', $data));
    }


    /**
     * Edit/Create home module.
     */
    public function edit()
    {
        $this->load->model('mobile/main');
        $this->load->language('mobile/main');
        $this->document->setTitle($this->language->get('title_edit_create'));

        $id = $this->request->get['id'] ?? 0;
        $module = $this->model_mobile_main->getHomeModule($id);
        if (!empty($module)) {
            $module['data'] = $this->getModuleEditableInfo($module);
        }

        $this->load->model('design/banner');

        $data = [
            'breadcrumbs' => [
                [
                    'text' => $this->language->get('text_home'),
                    'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true),
                ],
                [
                    'text' => $this->language->get('heading_title'),
                    'href' => $this->url->link('mobile/main', 'user_token=' . $this->session->data['user_token'], true),
                ],
                [
                    'text' => $this->language->get('title_edit_create'),
                    'href' => $this->url->link('mobile/main/edit', 'user_token=' . $this->session->data['user_token'], true),
                ],
            ],
            'id'          => $id,
            'user_token'  => $this->session->data['user_token'],
            'header'      => $this->load->controller('common/header'),
            'column_left' => $this->load->controller('common/column_left'),
            'footer'      => $this->load->controller('common/footer'),
            'banners'     => $this->model_design_banner->getBanners(),
            'module'      => $module,
            'action'      => $this->url->link('mobile/main/save', 'user_token=' . $this->session->data['user_token'], true),
            'type'        => empty($module) ? $this->request->get['type'] : $module['type'],
        ];

        $this->response->setOutput($this->load->view('mobile/edit', $data));
    }


    /**
     * Save home module changes.
     */
    public function save()
    {
        $this->load->model('mobile/main');
        $this->load->language('mobile/main');

        if ($this->request->get['id'] != 0) {
            $this->model_mobile_main->editHomeModule($this->request->get['id'], $this->request->post);
        } else {
            $this->model_mobile_main->addHomeModule($this->request->post);
        }

        $this->session->data['success'] = true;
        $this->response->redirect($this->url->link('mobile/main', 'user_token=' . $this->session->data['user_token'], true));
    }


    /**
     * Delete home module.
     */
    public function delete()
    {
        $this->load->model('mobile/main');
        $id = $this->request->get['id'] ?? 0;

        $this->session->data['success'] = !empty($this->model_mobile_main->getHomeModule($id));
        $this->model_mobile_main->deleteHomeModule($id);
        $this->response->redirect($this->url->link('mobile/main', 'user_token=' . $this->session->data['user_token'], true));
    }


    private function getModuleEditableInfo($module)
    {
        $data = $this->model_mobile_main->getContentArr((array) json_decode($module['data']), $module['type']);

        // PRODUCTS
        $this->load->model('catalog/product');
        foreach ($data['products'] as $key => $val) {
            $info = $this->model_catalog_product->getProduct($val);

            if ($info) {
                $data['products'][$key] = [
                    'product_id' => $info['product_id'],
                    'name'       => $info['name'],
                ];
            }
        }

        return $data;
    }

}
