<?php

abstract class MobileController extends Controller {

	const HTTP_VERSION = 'HTTP/1.1';
    const ALLOWED_HEADERS = [
        'Content-Type',
        'Accept',
        'X-Requested-With',
	];


	protected $data = [];

	protected $status_code = 200;


	protected function send()
	{
        $this->response->addHeader('Access-Control-Allow-Origin: *');
        $this->response->addHeader('Access-Control-Allow-Headers: ' . implode(',', self::ALLOWED_HEADERS));
        $this->response->addHeader($this->getHttpStatusMessage());
		$this->response->addHeader('Content-Type: application/json; charset=utf-8');
		
		$this->response->setOutput(json_encode($this->data));
	}


	private function getHttpStatusMessage()
    {
        return self::HTTP_VERSION . ' ' . $this->status_code . ' ' . ([
            '100' => 'Continue',
            '101' => 'Switching Protocols',
            '102' => 'Processing',
            '103' => 'Checkpoint',
            '200' => 'OK',
            '201' => 'Created',
            '202' => 'Accepted',
            '203' => 'Non-Authoritative Information',
            '204' => 'No Content',
            '205' => 'Reset Content',
            '206' => 'Partial Content',
            '207' => 'Multi-Status',
            '208' => 'Already Reported',
            '300' => 'Multiple Choices',
            '301' => 'Moved Permanently',
            '302' => 'Found',
            '303' => 'See Other',
            '304' => 'Not Modified',
            '305' => 'Use Proxy',
            '306' => 'Switch Proxy',
            '307' => 'Temporary Redirect',
            '308' => 'Permanent Redirect',
            '400' => 'Bad Request',
            '401' => 'Unauthorized',
            '402' => 'Payment Required',
            '403' => 'Forbidden',
            '404' => 'Not Found',
            '405' => 'Method Not Allowed',
            '406' => 'Not Acceptable',
            '407' => 'Proxy Authentication Required',
            '408' => 'Request Timeout',
            '409' => 'Conflict',
            '410' => 'Gone',
            '411' => 'Length Required',
            '412' => 'Precondition Failed',
            '413' => 'Request Entity Too Large',
            '414' => 'Request-URI Too Long',
            '415' => 'Unsupported Media Type',
            '416' => 'Requested Range Not Satisfiable',
            '417' => 'Expectation Failed',
            '418' => 'I\'m a teapot',
            '422' => 'Unprocessable Entity',
            '423' => 'Locked',
            '424' => 'Failed Dependency',
            '425' => 'Unassigned',
            '426' => 'Upgrade Required',
            '428' => 'Precondition Required',
            '429' => 'Too Many Requests',
            '431' => 'Request Header Fields Too Large',
            '451' => 'Unavailable for Legal Reasons',
            '500' => 'Internal Server Error',
            '501' => 'Not Implemented',
            '502' => 'Bad Gateway',
            '503' => 'Service Unavailable',
            '504' => 'Gateway Timeout',
            '505' => 'HTTP Version Not Supported',
            '506' => 'Variant Also Negotiates',
            '507' => 'Insufficient Storage',
            '508' => 'Loop Detected',
            '509' => 'Bandwidth Limit Exceeded',
            '510' => 'Not Extended',
            '511' => 'Network Authentication Required',
            '512' => 'Not updated',
            '521' => 'Version Mismatch',
        ][$this->status_code] ?? '');
	}

}