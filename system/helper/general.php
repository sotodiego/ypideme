<?php
function token($length = 32) {
	// Create random token
	$string = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

	$max = strlen($string) - 1;

	$token = '';

	for ($i = 0; $i < $length; $i++) {
		$token .= $string[mt_rand(0, $max)];
	}

	return $token;
}

/**
 * Backwards support for timing safe hash string comparisons
 *
 * http://php.net/manual/en/function.hash-equals.php
 */

if(!function_exists('hash_equals')) {
	function hash_equals($known_string, $user_string) {
		$known_string = (string)$known_string;
		$user_string = (string)$user_string;

		if(strlen($known_string) != strlen($user_string)) {
			return false;
		} else {
			$res = $known_string ^ $user_string;
			$ret = 0;

			for($i = strlen($res) - 1; $i >= 0; $i--) $ret |= ord($res[$i]);

			return !$ret;
		}
	}
}

if (!function_exists('echod')) {

    /**
     * Print a string and die
     */
    function echod(...$args): void
    {
        foreach ($args as $arg) {
            echo $arg;
        }

        die;
    }
}

if (!function_exists('printr')) {

    /**
     * Print the given values in a nice looking way
     */
    function printr(...$args): void
    {
        echo '<pre>';
        array_map('print_r', $args);
        echo '</pre>';
    }
}

if (!function_exists('printrd')) {

    /**
     * Print the given values in a nice looking way and die
     */
    function printrd(...$args): void
    {
        echo '<pre>';
        array_map('print_r', $args);
        echo '</pre>';
        die;
    }
}

if (!function_exists('dumpd')) {

    /**
     * Var dump the given values and die
     */
    function dumpd(...$args): void
    {
        array_map('var_dump', $args);
        die;
    }
}
